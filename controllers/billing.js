
const BillingModel = require("../models/billing")
function generateBillingId() {
    // Implement logic to generate a unique ID
    return Math.floor(Math.random() * 1000000) + 1; // Example placeholder
}

const generateBilling =  (async (req, res) => {
    const { payment_status, amount, patient_id,payment_date, doctor_id, charge_id = null } = req.body;
    const bill_id = generateBillingId();
    const newBilling = new BillingModel({
        bill_id,
        payment_status,
        payment_date,
        amount,
        patient_id,
        doctor_id,
        charge_id,
    });
    const response= await newBilling.save();
    res.status(200).json(response)
})



module.exports={
    generateBilling
}