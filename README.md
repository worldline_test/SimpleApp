
# Setting Up a CI/CD Pipeline for a Simple Standalone Application using Gitlab and Jenkins

The objective of this assignment is to create a basic Continuous Integration and Continuous Deployment (CI/CD) pipeline using GitLab and Jenkins for a standalone application.






## 
## Setup GitLab Repository with name SimpleApp:

![App Screenshot](https://github.com/AayushAnand07/Assignment_QuizApp/assets/41218074/cad706e8-a65a-416a-9958-e0acd76b1a24)


#
## Setuped GitLab Repository with a Readme.md File

![App Screenshot](https://github.com/AayushAnand07/Assignment_QuizApp/assets/41218074/863b2ceb-a0a3-4446-ba0b-ee12575726fc)

#

## pushing the project code from local to remote gitlab repository

![App Screenshot](https://github.com/AayushAnand07/Assignment_QuizApp/assets/41218074/7de3c12a-88a6-4787-a312-6c5865cef1aa)

#

## Pushed all the files and moudules successfully of the Application

![App Screenshot](https://github.com/AayushAnand07/Assignment_QuizApp/assets/41218074/af085bdc-a662-4417-8020-1271c64ef822)

#
#
#

# Creating Jenkins Pipeline: 
#

## Starting Jenkins locally and configuring a new Pipeline Project with Gitlab remote Url of the application 

![App Screenshot](https://github.com/AayushAnand07/Assignment_QuizApp/assets/41218074/6c26b250-0eeb-4593-9da4-8cddbb4472f1)


#
## Saving the Configuration of Jenkins Pipeline through JenkinFile script for pipeline stages 

![App Screenshot](https://github.com/AayushAnand07/Assignment_QuizApp/assets/41218074/5b8d6e78-1d30-4a7b-8310-b9406fe4fcc6)


#
## Creating JenkinFile in Gitlab repo to build and deploy project through Jenkins Pipeline

![App Screenshot](https://github.com/AayushAnand07/Assignment_QuizApp/assets/41218074/bf2c0661-306d-4c14-b9fd-f522c18bc3bf)

#
#
#

# Setting up Gitlab CI configuration: 

#
## Setting Up Continuous Intergration from Gitlab Project Reposittory by integrating it with Jenkins to listen for commits and build and deploy application through Jenkins 

![App Screenshot](https://github.com/AayushAnand07/Assignment_QuizApp/assets/41218074/6afabc26-df13-4c16-bac8-3c4f53365348)

#
## Creating public address for local Jenkins Server using Ngrok to be used with gitlab integration 

![App Screenshot](https://github.com/AayushAnand07/Assignment_QuizApp/assets/41218074/5547a705-706b-4b9e-8347-db37e9eaa363)


![App Screenshot](https://github.com/AayushAnand07/Assignment_QuizApp/assets/41218074/7264cae9-37ec-4ef5-827c-79ba86c0d6c4)


#
## Integration is setuped successfully

![App Screenshot](https://github.com/AayushAnand07/Assignment_QuizApp/assets/41218074/5ce4138e-894e-442f-8e64-b04213030234)


#
#
# 


# Testing the CI/CD Pipeline built from Gitlab to Jenkins

## Commiting Changes in gitlab Repository

![App Screenshot](https://github.com/AayushAnand07/Assignment_QuizApp/assets/41218074/f71ac6e6-35b1-456e-94ff-a56e19d6ad6e)

#
## Automatic Triggered build event in Jenkins based on commits in Gitlab Repository

![App Screenshot](https://github.com/AayushAnand07/Assignment_QuizApp/assets/41218074/12e0e30f-5f6b-44f2-94f3-91e4019cd72d)

![App Screenshot](https://github.com/AayushAnand07/Assignment_QuizApp/assets/41218074/e35f7b7f-0514-4af0-beef-84ec6818a163)

![App Screenshot](https://github.com/AayushAnand07/Assignment_QuizApp/assets/41218074/b44decb6-a0ac-48b9-8f36-8cda08382f55)

#
#
## Node Server Running successfully using pm2

![App Screenshot](https://github.com/AayushAnand07/Assignment_QuizApp/assets/41218074/4090bd83-9f26-4447-8338-0750709fafe3)

#
## Testing the Server deployed
![App Screenshot](https://github.com/AayushAnand07/Assignment_QuizApp/assets/41218074/ca1454d0-5c5f-4058-b407-d3f88abdd963)







