const express = require("express")
const {generateChargeSheet,updateBillId}= require('../controllers/chargesheet')
const router = express.Router()

router.post('/chargesheet/generate',generateChargeSheet )
router.put('/chargesheet/updateBill/:chargeId',updateBillId)

    // router.post('/api/auth/login',login)
module.exports= router
